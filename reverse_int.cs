//Given a 32-bit signed integer, reverse digits of an integer.
//Example 1: 
//Input: 123
//Output:  321

//Example 2: 
//Input: -123
//Output: -321

//Example 3: 
//Input: 120
//Output: 21

//Note:
//Assume we are dealing with an environment which could only hold integers within
//the 32-bit signed integer range. For the purpose of this problem, assume that
//your function returns 0 when the reversed integer overflows. 
 
    public int Reverse(int x)
    {
        bool negative = x < 0;
        bool f = false;

        if (negative)
        {
            x = Math.Abs(x);
            f = true;
        }
        string d = x.ToString();
        char[] arr = d.ToCharArray();
        int begin = 0;
        int end = arr.Length - 1;
        char temp;
        int num = 0;



        while (begin < end)
        {
            temp = arr[begin];
            arr[begin] = arr[end];
            arr[end] = temp;
            begin++;
            end--;
        }

        String b = new String(arr);
        Console.WriteLine(arr);
        string q = b.ToString();

        try
        {
            num = checked(Convert.ToInt32(q));
            if (f == true)
            {
                num = num * -1;
            }
        }
        catch (System.OverflowException e)
        {
            return 0;
        }
        return num;

    }