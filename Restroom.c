/*-
This program simulates the Unisex restroom problem provided by the textbook. This solution allows all users to equally share the
restroom.
CSC389 - Project 2
Author: Brian Pickard
Date: 12/04/2016
*/
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#define NTHREADS 10
int first = 0, last = 0, qSize = 0, f = 0, fInRoom = 0, mInRoom = 0, cycle = 0;
void * a[NTHREADS];
sem_t mMutex;
sem_t fMutex;
sem_t restroom_empty;
sem_t m_limit;
sem_t f_limit;
int counter, mCounter, fCounter = 0;

void push(void * x)
{
	// put thread ID into the line and increment cycle counter
	if (first == 0)
	{
		printf("Line is empty\n");
	}

	a[first] = x;
	first = (first + 1) % NTHREADS;
	qSize++;
	printf("Cycle %d\n", cycle);
	cycle++;
}
int pop()
{
	//Take the thread ID out of line
	int Qnum;

	Qnum = a[last];
	last = (last + 1) % NTHREADS;
	qSize--;

	return Qnum;

}
void man_wants_to_enter(void *mID)
{
	sem_wait(&mMutex);

	mCounter++;
	if (mCounter == 1)
	{
		//Make the restroom open to male threads only
		sem_wait(&restroom_empty);
		printf("Males are using the restoom\n");
	}
	//Push the male thread into the line
	push(mID);


	printf("Male %d is in line\n", mID);
	sem_post(&mMutex);
}
void man_leaves(void *mID)
{

	sem_wait(&mMutex);
	printf("Cycle %d\n", cycle);
	cycle++;
	printf("Male %d is leaving\n", mID);
	mCounter--;

	// If the counter is 0 restroom is empty allow women to use it
	if (mCounter == 0)
	{
		sem_post(&restroom_empty);
	}

	sem_post(&mMutex);
}
void woman_wants_to_enter(void *fID)
{
	sem_wait(&fMutex);

	fCounter++;

	if (fCounter == 1)
	{
		sem_wait(&restroom_empty);
		printf("Females are using the restoom\n");
	}
	push(fID);

	printf("Female %d is in line\n", fID);


	sem_post(&fMutex);
}
void woman_leaves(void *fID)
{
	sem_wait(&fMutex);
	printf("Cycle %d\n", cycle);
	cycle++;
	printf("Female %d is leaving\n", fID);
	fCounter--;

	// If the counter is 0 restroom is empty allow men to use it
	if (fCounter == 0)
	{
		sem_post(&restroom_empty);
	}

	sem_post(&fMutex);
}

void limit_mRestroom(void *mID)
{

	int num;
	// Allow only three male threads in at a time
	sem_wait(&m_limit);
	// If aviliable take man out of line
	num = pop();
	// Enter critical region to increment cycle
	sem_wait(&mMutex);
	printf("Cycle %d\n", cycle);
	cycle++;
	sem_post(&mMutex);

	printf("Male %d is out of line\n", num);

	mInRoom++;
	printf("Male %d is entering\n", mID);

	printf("There are %d males using the restroom\n", mInRoom);
	// male is using the restroom
	sleep(3);
	printf("Male %d is finishing up\n", mID);

	mInRoom--;
	// Next men in line can enter
	sem_post(&m_limit);
}
void limit_fRestroom(void *fID)
{
	int num;
	//Allow three females at a time
	sem_wait(&f_limit);
	// If aviliable take woman out of line
	num = pop();
	// Enter critical region to increment cycle
	sem_wait(&fMutex);
	printf("Cycle %d\n", cycle);
	cycle++;
	sem_post(&fMutex);

	printf("Female %d is out of line\n", num);
	fInRoom++;

	printf("Female %d is entering\n", fID);

	printf("There are %d females using the restroom\n", fInRoom);
	// female is using the restroom
	sleep(3);

	printf("Female %d is finishing up\n", fID);
	fInRoom--;
	// Next woman in line can enter
	sem_post(&f_limit);
}

void *male(void *mID)
{
	man_wants_to_enter(mID);
	limit_mRestroom(mID);
	man_leaves(mID);

}
void *female(void *fID)
{



	woman_wants_to_enter(fID);
	limit_fRestroom(fID);
	woman_leaves(fID);


}

int main()
{
	pthread_t thread_id[NTHREADS];
	int i, j;
	sem_init(&mMutex, 0, 1);
	sem_init(&fMutex, 0, 1);
	sem_init(&restroom_empty, 0, 1);
	sem_init(&m_limit, 0, 3);
	sem_init(&f_limit, 0, 3);

	// Create the male and female Pthreads
	for (i = 0; i < NTHREADS; i++)
	{
		pthread_create(&thread_id[i], NULL, male, (void *)i);
		pthread_create(&thread_id[i], NULL, female, (void *)i);

	}
	// wait for threads to complete. 
	for (j = 0; j < NTHREADS; j++)
	{
		pthread_join(thread_id[j], NULL);
	}

	return 0;
}