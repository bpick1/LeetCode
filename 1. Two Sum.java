﻿//Given an array of integers, return indices of the two numbers such that they add up to a specific target.
//You may assume that each input would have exactly one solution, and you may not use the same element twice

    public int[] twoSum(int[] nums, int target)
    {
        int[] a1 = new int[2];
        int[] a2 = Arrays.copyOf(nums, nums.length);

        int first = 0;
        int second = 0;
        boolean a1notdone = true;
        boolean a2notdone = true;




        Arrays.sort(nums);


        for (int i = 0; i < nums.length; i++)
        {
            int difference = target - nums[i];
            if (Arrays.binarySearch(nums, i + 1, nums.length, difference) > 0)
            {

                first = nums[i];
                second = nums[Arrays.binarySearch(nums, i + 1, nums.length, difference)];
                break;

            }
        }
        System.out.println(first + " " + second);

        for (int i = 0; i < a2.length; i++)
        {
            if (a2[i] == first && a1notdone)
            {
                a1[0] = i;
                a1notdone = false;
            }
            else if (a2[i] == second && a2notdone)
            {
                a1[1] = i;
                a2notdone = false;
            }
        }
        return a1;

    }
