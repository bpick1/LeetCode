//Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1. 
public class Solution 
{
    public int FirstUniqChar(string s) 
    {
            char[] x = s.ToCharArray();
            List<char> list = new List<char>();
            Dictionary<char, int> dic = new Dictionary<char, int>();
        
            for (int i = 0; i < s.Length; i++)
            {
                if (!dic.ContainsKey(x[i]))
                {
                    dic.Add(x[i], i);
                }
                else
                    dic[s[i]] = -1;
            }

            foreach (int value in dic.Values)
            {
                if (value != -1)
                {
                    return value;
                }
            }
        return -1;
    }
}