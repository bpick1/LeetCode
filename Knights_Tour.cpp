/*****************************************************************************
TITLE: KnightsTour.cpp  Homework Assignment 1
AUTHOR: Brian Pickard BP
CREATE DATE: 2.1.2013
PURPOSE: This C++ program completes the knights tour using a heuristic algorithm
for the first 32 moves. The final movves are computed using a stack and
an iterative backtracking procedure. A full detailed explanation is
provided in the report for this assignment.

LAST MODIFIED ON: 2.18.2013
LAST MODIFIED BY: Brian Pickard BP
******************************************************************************/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stack>
#include <list> 

using namespace std;

// Function prototypes
void reset(int[][8]);
void warnsdoff(list<int>, int, int[][8]);
void useStack(int, int, int, int[][8]);
bool validMove(int, int, int[][8]);

/*The main function of the program*/
int main()
{
	// 8x8 chessboard
	int chessBoard[8][8];

	// Sequence number of the moves made
	int moveNumber = 0;

	char choice;

	char input;

	bool switchFlag = false;

	bool keepAsking = false;

	bool finish = false;
	// X coordinate
	int x;
	// Y coordinate
	int y;
	// The Link List using STL <list>
	list<int>initalCond;
	// Link List iterator
	list<int>::iterator i;

	// Data entry mode.. add x and y coordinates to the link list.
	while (!keepAsking)
	{

		cout << "Enter X condition between 0-7" << endl;
		cin >> x;
		while (x < 0 || x > 7)
		{
			cout << "Invalid Input please try again" << endl;
			cout << "Enter X condition between 0-7" << endl;
			cin >> x;
		}
		initalCond.push_back(x);
		cout << "Enter Y condition between 0-7" << endl;
		cin >> y;
		while (y < 0 || y > 7)
		{
			cout << "Invalid Input please try again" << endl;
			cout << "Enter Y condition between 0-7" << endl;
			cin >> y;
		}
		initalCond.push_back(y);
		cout << "Enter more conditions? (Type n to exit type anything else to continue)" << endl;
		cin >> choice;

		if (choice == 'n' && 'N')
			keepAsking = true;
	}

	cout << "Printing out linked list" << endl;
	for (list< int >::iterator j = initalCond.begin(); j != initalCond.end(); ++j)
	{
		cout << " " << *j;
	}

	cout << "\n" << endl;

	// Prompt for additional options before starting the tour allows users to 
	// add,delete, and modify
	while (!switchFlag)
	{
		cout << "Do you want to change the inital positions?" << endl;
		cout << "a. Add condition\n";
		cout << "b. Delete condition\n";
		cout << "c. Modify condition\n";
		cout << "d. Proceed with Knights Tour\n";
		cout << "Selection: ";
		cin >> input;

		switch (input)
		{
			// Add a condition to link list
		case 'a':
		case 'A':
			cout << "Enter X corrdinate between 0-7" << endl;
			cin >> x;
			while (x < 0 || x > 7)
			{
				cout << "Invalid Input please try again" << endl;
				cout << "Enter X condition between 0-7" << endl;
				cin >> x;
			}
			cout << "Enter Y coordinate between 0-7" << endl;
			cin >> y;
			while (y < 0 || y > 7)
			{
				cout << "Invalid Input please try again" << endl;
				cout << "Enter Y condition between 0-7" << endl;
				cin >> y;
			}
			initalCond.push_back(x);
			initalCond.push_back(y);
			cout << "Printing out linked list" << endl;

			for (list< int >::iterator ii = initalCond.begin(); ii != initalCond.end(); ++ii)
			{
				cout << " " << *ii;

			}
			cout << "\n" << endl;
			break;

			// Delete x and y values from link list
		case 'b':
		case 'B':
			initalCond.pop_back();
			initalCond.pop_back();
			cout << "Printing out linked list" << endl;
			for (list< int >::iterator k = initalCond.begin(); k != initalCond.end(); ++k)
			{
				cout << " " << *k;
			}
			cout << "\n" << endl;
			break;
			// Modify the link list by deleting the element and adding a new one
		case 'c':
		case 'C':

			int element;
			int newElement;
			i = initalCond.begin();
			cout << "Select an element to modify" << endl;
			cin >> element;

			advance(i, element);
			i = initalCond.erase(i);


			cout << "Enter a corrdinate between 0-7" << endl;
			cin >> newElement;
			while (newElement < 0 || newElement > 7)
			{
				cout << "Invalid Input please try again" << endl;
				cout << "Enter X condition between 0-7" << endl;
				cin >> newElement;
			}

			initalCond.insert(i, newElement);

			break;
			// Continue with the program break out of switch statement
		case 'd':
		case 'D':
			switchFlag = true;
			break;

		default:
			cout << "Error, bad input, try again\n";

		}
	}
	cout << "Printing out linked list" << endl;
	for (list< int >::iterator i = initalCond.begin(); i != initalCond.end(); ++i)
	{
		cout << " " << *i;
	}
	cout << "\n" << endl;
	// Loop starts main logic of the program. Solves Knights Tour
	while (!finish)
	{
		cout << "Printing out first 32 moves" << endl;
		// Start warnsdoff's function
		warnsdoff(initalCond, moveNumber, chessBoard);
		initalCond.pop_back();
		initalCond.pop_back();
		// Reset the board
		reset(chessBoard);
		if (initalCond.empty())
		{
			finish = true;
		}

	}



}
/* Solves warnsdoffs heuristic for first half of moves using an accisbility table
-3 parameters- returns nothing */
void warnsdoff(list<int> firstMoves, int moveNumber, int chessBoard[][8])
{
	// Possible horizontal moves for knight
	int horizontal[8] = { -2,-1, 1, 2, 2, 1, -1,-2 };
	// Possible vertical moves for knight
	int vertical[8] = { 1, 2, 2, 1,-1,-2,-2,-1 };
	// Accessibility table
	int moveBoard[8][8] = { { 2,3,4,4,4,4,3,2 },
	{ 3,4,6,6,6,6,4,3 },
	{ 4,6,8,8,8,8,6,4 },
	{ 4,6,8,8,8,8,6,4 },
	{ 4,6,8,8,8,8,6,4 },
	{ 4,6,8,8,8,8,6,4 },
	{ 3,4,6,6,6,6,4,3 },
	{ 2,3,4,4,4,4,3,2 } };
	// Variables for moving the knight and accessing the accessibility table
	int x, y, newRow, newCol, minRow, minCol, minAccess = 9, accessNumber, moveType;

	int line = 0;

	bool finished = false;

	// Clears the board and sets all positions to -1
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			chessBoard[i][j] = -1;
		}
	}
	// Get the values from the link list and then pop them from the list
	x = firstMoves.back();
	firstMoves.pop_back();
	y = firstMoves.back();
	firstMoves.pop_back();

	chessBoard[x][y] = ++moveNumber;
	cout << "Solving using inital positions " << y << ", " << x << endl;
	// Compute the tour
	while (!finished)
	{
		accessNumber = minAccess;
		// Go through all the moves
		for (moveType = 0; moveType <= 7; moveType++)
		{
			if (moveType > 7)
				break;
			newRow = x + vertical[moveType];
			newCol = y + horizontal[moveType];
			// Check if moves are valid
			if (validMove(newRow, newCol, chessBoard))
			{
				// Check moves with accessibility table with lowest number of moves
				if (moveBoard[newRow][newCol] < accessNumber)
				{
					accessNumber = moveBoard[newRow][newCol];
					minRow = newRow;
					minCol = newCol;
				}
				--moveBoard[newRow][newCol];
			}
		}
		// When half of moves are complete stop the function
		if (moveNumber == 32)
		{
			finished = true;
		}
		// Set the knight to its location and increse  the move number
		else
		{
			x = minRow;
			y = minCol;
			chessBoard[x][y] = ++moveNumber;
		}
	}
	// Print out the chessboard
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			printf("%d\t", chessBoard[i][j]);
			line++;

			if (line == 8)
			{
				printf("\n");
				line = 0;
			}
		}
	}

	cout << "Printing out remaining moves" << endl;
	useStack(x, y, moveNumber, chessBoard);


}
/* Solves the remaining moves using a stack and backtracking
- 4 parameters - returns nothing */
void useStack(int x, int y, int moveNumber, int chessBoard[][8])
{
	// Use three stacks to store row column and move info
	stack<int>currentRow;
	stack<int>currentCol;
	stack<int>currentMoveType;

	// Possible horizontal moves for knight
	int horizontal[8] = { -2,-1, 1, 2, 2, 1, -1,-2 };
	// Possible vertical moves for knight
	int vertical[8] = { 1, 2, 2, 1,-1,-2,-2,-1 };
	int newRow, newCol, moveType = 0, line = 0;

	bool finished = false;
	// Push the previous data into the stack
	currentRow.push(x);
	currentCol.push(y);
	currentMoveType.push(0);
	chessBoard[x][y] = ++moveNumber;

	bool goodMove = false;
	// Compute the tour
	while (!finished)
	{
		// Go through all the moves
		for (int count = 0; count <= 7; count++)
		{
			if (moveType > 7)
				break;
			newRow = x + vertical[moveType];
			newCol = y + horizontal[moveType];
			// Check if move is valid
			goodMove = validMove(newRow, newCol, chessBoard);
			// If the move is good pop the stack and add the new movetype
			// then push the new stack info and place the knight
			if (goodMove)
			{
				currentMoveType.pop();
				currentMoveType.push(moveType);


				x = newRow;
				y = newCol;
				currentRow.push(x);
				currentCol.push(y);
				currentMoveType.push(0);
				chessBoard[x][y] = ++moveNumber;
				moveType = 0;
				break;

			}
			moveType++;
		}
		// If the move is not good use backtraking using the stack
		if (!goodMove)
		{
			bool stackGood = true;
			// Check stack conditions
			// While the stack is good get new coordinates and pop stack data and finally backtrack
			while (stackGood)
			{
				x = currentRow.top();
				y = currentCol.top();
				chessBoard[x][y] = -1;
				currentRow.pop();
				if (!currentRow.empty())
				{
					x = currentRow.top();
				}
				else
				{
					cout << "Solution not found" << endl;
					stackGood = false;
				}
				currentCol.pop();
				if (!currentCol.empty())
				{
					y = currentCol.top();
				}
				else
				{
					cout << "Soulution not found" << endl;
					stackGood = false;
				}
				currentMoveType.pop();
				if (!currentMoveType.empty())
				{
					moveType = currentMoveType.top() + 1;
				}

				if (moveType <= 7)
					stackGood = false;
				if (moveNumber > 1)
					moveNumber--;
				else
				{
					cout << "Error" << endl;
				}

			}

		}

		// If all the moves are made stop the function
		if (moveNumber == 65)
		{
			finished = true;
		}
	}
	// Print out the chessboard
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			printf("%d\t", chessBoard[i][j]);
			line++;

			if (line == 8)
			{
				printf("\n");
				line = 0;
			}
		}
	}
}


/* Resets the board
- 1 parameter - returns nothing*/
void reset(int chessBoard[][8])
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			chessBoard[i][j] = -1;
		}
	}
}
/* Checks if the move is a valid move
- 3 parameters - returns boolean*/
bool validMove(int x, int y, int chessBoard[][8])
{
	// The move has to be made on the chessboard and has to land on an empty space
	// If sucessful it returns true
	return (x >= 0 && x <= 7 && y >= 0 && y <= 7 && chessBoard[x][y] == -1);
}